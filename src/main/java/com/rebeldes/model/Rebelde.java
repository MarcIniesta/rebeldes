package com.rebeldes.model;

//modelo del rebelde
public class Rebelde {
	Integer id;
	String nombre;
	String planeta;
	String fecha;
	
	//Contructor del rebelde
	public Rebelde() {
		super();
	}
	
	//Contructor con todos los parametros
	public Rebelde(Integer id,String nombre,String planeta, String fecha) {
		this.id = id;
		this.nombre = nombre;
		this.planeta = planeta;
		this.fecha = fecha;
	}
	
	//seters y geteres
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getPlaneta() {
		return planeta;
	}
	
	public void setPlaneta(String planeta) {
		this.planeta = planeta;
	}
	
	public String getFecha() {
		return fecha;
	}
	
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}
