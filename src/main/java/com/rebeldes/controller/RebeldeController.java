package com.rebeldes.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rebeldes.model.Rebelde;


@RestController
@RequestMapping("api")
public class RebeldeController {
	
	//Lista todos los ususarios
	  @RequestMapping(value = "rebeldes", method = RequestMethod.GET)
	  public List<Rebelde> list(){
	    return RebeldesBBDD.list();
	  }
	  
	  //Crea un nuevo usuario 
	  @RequestMapping(value = "rebeldes", method = RequestMethod.POST)
	  public Rebelde create(@RequestBody Rebelde rebelde){
	    return RebeldesBBDD.create(rebelde);
	  }
	  
	  //filtra usario por id como parametro
	  @RequestMapping(value = "rebeldes/{id}", method = RequestMethod.GET)
	  public Rebelde get(@PathVariable Integer id){
	    return RebeldesBBDD.get(id);
	  }
	  
	  //Actualiza usuario por id como parametro
	  @RequestMapping(value = "rebeldes/{id}", method = RequestMethod.PUT)
	  public Rebelde update(@PathVariable Integer id, @RequestBody Rebelde rebelde){    
	    return RebeldesBBDD.update(id, rebelde);
	  }
	  
	  //Elemina usuario por id como parametro
	  @RequestMapping(value = "rebeldes/{id}", method = RequestMethod.DELETE)
	  public Rebelde delete(@PathVariable Long id){
	    return RebeldesBBDD.delete(id);
	  }


}
