package com.rebeldes.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.rebeldes.model.Rebelde;

public class RebeldesBBDD {
	//crea el mapeado
	  private static Map<Integer, Rebelde> entradas = new HashMap<Integer, Rebelde>();
	  private static int idIndex = 3;
	  
	  //Añade campos por defecto
	  static {
		  Rebelde RebeldeA = new Rebelde(1,"Marc","Tierra","2014-09-15");
		  Rebelde RebeldeB = new Rebelde(2,"Alien","Jupiter","2000-10-01");
		  Rebelde RebeldeC = new Rebelde(3,"Alien 2","Marte","2014-09-4");
		  
		  entradas.put(1,RebeldeA);
		  entradas.put(2,RebeldeB);
		  entradas.put(3,RebeldeC);

	  }
	  
	  //Lista todas las entradas
	  public static List<Rebelde> list() {
		  return new ArrayList<Rebelde>(entradas.values());
	  }

	  //crea un nueva entrada
	  public static Rebelde create(Rebelde entrada) {
		    idIndex = idIndex+1;
		    entrada.setId(idIndex);
		    entradas.put(idIndex, entrada);
		    return entrada;
	  }
	  
	  //recoje un id para filtrar
	  public static Rebelde get(Integer id) {
		    return entradas.get(id);
	  }
	  
	  //actualiza un rebelde por su id
	  public static Rebelde update(Integer id, Rebelde entrada) {
		entradas.put(id, entrada);
	    return entrada;
	  }
	  
	  //elimina una entrara
	  public static Rebelde delete(Long id) {
	    return entradas.remove(id);
	  }
		  
}
