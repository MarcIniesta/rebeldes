        function ListarUsuarios() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var usuarios = JSON.parse(this.responseText);
                    document.getElementById("ul").innerHTML="";
                    for (let i = 0; i < usuarios.length; i++) {
                        var ul = document.getElementById("ul");
                        var usuario = document.createElement("li");
                        usuario.innerHTML = "Rebelde <b>"+usuarios[i].nombre + "</b> de <b>"+usuarios[i].planeta + "</b> el <b>"+usuarios[i].fecha+"</b>";

                        ul.appendChild(usuario);


                    }
                    
                }
            };
            xhttp.open("GET", "http://localhost:8080/api/rebeldes", true);
            xhttp.setRequestHeader("Content-type", "application/json");
            xhttp.send("Your JSON Data Here");
        }
        
        function addUsuario() {
        	var id;
            var nombre = document.getElementById("nombre").value;
            var planeta = document.getElementById("planeta").value;
            var fecha = document.getElementById("fecha").value;

            if(!nombre || !fecha){
            	$('.message').removeClass("oculto");
            	return false;
            }else{
            	$('.message').addClass("oculto");
            }


            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var usuarios = JSON.parse(this.responseText);
                    id = usuarios.length +1;
                    ListarUsuarios();
                    
                }
            };
            xhttp.open("POST", "http://localhost:8080/api/rebeldes", true);
            xhttp.setRequestHeader("Content-type", "application/json");

            
            var datos =JSON.stringify({
            	'id' : id,
                'nombre': nombre,
                'planeta': planeta,
                'fecha': fecha
              });
            console.log("ADD", datos);
            xhttp.send(datos);
            
        }